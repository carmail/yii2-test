# README #

Please read everything before you start with the test.

## What this test is for? ##

Purpose of this test is to make sure the developer is familiar with what he says like git/bitbucket/github Yii2 Framework, PHP, Responsive webpage design.


### How do I get set up? ###

- Start with fresh yii2 framework
- Create a DB migration files based on the csv files in inputs folder
- Seed the tables with records provided in those csv files

 ***Pleas note Relation between those files are on dealers.INVENTORY_URL= contacts.dealersite you may use this to join dealers table with contacts to show seller_name in grid view when you show contacts***

### What is the task? ###

- Create simple login page
- CRUD Page with search filters for both dealers and contacts
- Added a dummy user with below credentials

```
username : Test
password: Test!@#qwe1 
```

- Commmit yours changes with comments
- Add the steps or instructions you require to succesfully deploy the project in README.md 

for eg 
```
compose install
yii migrate
etc
```

### Pack your project using git bundle command ###

```
git bundle create username-date
```

send the bundle to upwork chat or other channel you received this test on.

### Who do I talk to in case of any question? ###

Though the test is pretty straight forward but you may use the Same channel you got the test on.